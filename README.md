## Installation

You can install the package via composer:

```bash
composer require sandi/flysystem-one-drive
```

This is sample of config filesystem disc:

```php
 [

    'one-drive-sample' => [
            'driver' => 'one-drive',
            'path_prefix' => env('ONE_DRIVE_PATH_PREFIX', 'root')
            // optional
            'tenanet_id' => env('ONE_DRIVE_TENANET_ID'),
            'app_id' => env('ONE_DRIVE_APP_ID'),
            'app_key' => env('ONE_DRIVE_APP_KEY'),
            // or set token
            'access_token' => env('ONE_DRIVE_ACCESS_TOKEN'),
        ],

];
```

usage

add to composer json you private ropository
```json
{
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:***_sandi/flysystem-one-drive.git"
        }
    ]
}
```
then run
```bash
composer require sandi/flysystem-one-drive
```

```php

// add to routes
$router->get('test', function () {
    $storage = \Illuminate\Support\Facades\Storage::disk('one-drive-sample');
    dd($storage->files('AppOneDrive'));
});

```
result
```php
 [
  "AppOneDrive/123.txt"
]
```