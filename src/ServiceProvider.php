<?php

namespace Sandi\FlysistemOneDrive;

use League\Flysystem\Filesystem;
use Microsoft\Graph\Graph;
use NicolasBeauvais\FlysystemOneDrive\OneDriveAdapter;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['filesystem']->extend('one-drive', function ($app, $config) {

            $graph = new Graph();
            $graph->setAccessToken($this->acquireToken($config));

            $adapter = new OneDriveAdapter($graph, $config['path_prefix'] ?? 'root');
            $filesystem = new Filesystem($adapter);

            return $filesystem;
        });
    }

    protected function acquireToken(array $config): ?string
    {
        return $config['access_token'] ?? $this->acquireAppToken($config);
    }

    protected function acquireAppToken(array $config): ?string
    {
        $guzzle = new \GuzzleHttp\Client();
        $tenantId = $config['tenanet_id'];
        $clientId = $config['app_id'];
        $clientSecret = $config['app_key'];
        $url = 'https://login.microsoftonline.com/' . $tenantId . '/oauth2/token';

        $token = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'resource' => 'https://graph.microsoft.com/',
                'grant_type' => 'client_credentials',
            ],
        ])->getBody()->getContents());

        return $token->access_token ?? null;
    }
}
